# Boxinator-Back-End

The project is a Web API for the full Boxinator project. Through the API you can create new shipments, countries and users, view all the information from shipments, the shipments created by the user and edit the countries and shipment status. The api is also documented for a more user-friendly use. 

## Table of Contents

- [Clone](#clone)
- [Install](#install)
- [Start](#start)
- [Usage](#usage)
- [Contributers](#contributers)

## Clone

You can clone this repository for your own uses through the following command in your terminal:
```
cd <your-chosen-folder>
git clone https://gitlab.com/oljojo/boxinator-back-end.git
```

## Install

After downloading the code, open the solution (Boxinator.sln) with Visual Studio.

To seed the database with the seed-data created, the following command must be executed in the "Package Manager Console" of Visual Studio:
```
update-database -context BoxinatorDbContext
```

## Start

Once the solution is opened in Visual Studio it can easily be run through Visual studio. A server should be started and the browser be opened automatically.

## Usage

The software will start a Web API server with Swagger included to easily run the requests that can be executed. The paths are separated based on Countries, Shipments and Users, all with separate endpoints for GET, POST, PUT and DELETE and some of the controllers have special endpoints.

## Known bugs/Future development
* No tests are created for the project

## Contributers
- Jeremy - @Jerry585
- Olof - @oljojo
- Usko - @usk1129
