﻿using AutoMapper;
using Boxinator.DTOs.CountryDTO;
using Boxinator.Entity.Models;

namespace Boxinator.DTOs.Profiles
{
    public class CountryProfile : Profile
    {
        public CountryProfile()
        {
            CreateMap<CountryCreateDTO, Country>()
                .ForMember(c => c.Shipments, opt => opt
                .Ignore())
                .ReverseMap();

            CreateMap<Country, CountryReadDTO>()
                .ForMember(cdto => cdto.Id, opt => opt
                .MapFrom(c => c.Id))
                .ForMember(cdto => cdto.Name, opt => opt
                .MapFrom(c => c.Name))
                .ForMember(cdto => cdto.Rate, opt => opt
                .MapFrom(c => c.Rate))
                .ReverseMap();

            CreateMap<CountryEditDTO, Country>()
                .ForMember(c => c.Name, opt => opt
                .MapFrom(cdto => cdto.Name))
                .ForMember(c => c.Rate, opt => opt
                .MapFrom(cdto => cdto.Rate))
                .ReverseMap();
        }
    }
}
