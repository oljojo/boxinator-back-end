﻿using AutoMapper;
using Boxinator.DTOs.StatusDTO;
using Boxinator.Entity.Models;

namespace Boxinator.DTOs.Profiles
{
    public class StatusProfile : Profile
    {
        public StatusProfile()
        {
            CreateMap<Status, StatusReadDTO>();
        }
    }
}
