﻿using AutoMapper;
using Boxinator.DTOs.TierDTO;
using Boxinator.Entity.Models;

namespace Boxinator.DTOs.Profiles
{
    public class TierProfile : Profile
    {
        public TierProfile()
        {
            CreateMap<Tier, TierReadDTO>();
        }
    }
}
