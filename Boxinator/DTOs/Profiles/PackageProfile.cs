﻿using AutoMapper;
using Boxinator.DTOs.PackageDTO;
using Boxinator.Entity.Models;

namespace Boxinator.DTOs.Profiles
{
    public class PackageProfile : Profile
    {
        public PackageProfile()
        {
            CreateMap<PackageCreateDTO, Package>();
            CreateMap<PackageEditDTO, Package>();
            CreateMap<Package, PackageReadDTO>();
        }
    }
}
