﻿using AutoMapper;
using Boxinator.DTOs.UerDTO;
using Boxinator.DTOs.UserDTO;
using Boxinator.Entity.Models;

namespace Boxinator.DTOs.Profiles
{
    public class UserProfile : Profile
    {

        public UserProfile()
        {
            CreateMap<UserCreateDTO, User>()
                .ForMember(cdto => cdto.Shipments, opt => opt
                .MapFrom(c => c.ShipmentIds.Select(m => new Shipment { Id = m }).ToList())).ReverseMap();

            CreateMap<User, UserReadDTO>()
                .ForMember(cdto => cdto.ShipmentIds, opt => opt
                .MapFrom(m => m.Shipments.Select(c => c.Id).ToArray()))
                .ReverseMap();
            
            CreateMap<UserEditDTO, User>()
                .ForMember(cdto => cdto.Shipments, opt => opt
                .MapFrom(m => m.ShipmentIds)).ReverseMap();
        }


    }
}
