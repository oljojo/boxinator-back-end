﻿using AutoMapper;
using Boxinator.DTOs.ShipmentDTO;
using Boxinator.Entity.Models;

namespace Boxinator.DTOs.Profiles
{
    public class ShipmentProfile: Profile
    {
        public ShipmentProfile()
        {
            CreateMap<Shipment, ShipmentReadDTO>();
            CreateMap<ShipmentEditDTO, Shipment>()
                .ForMember(s => s.Packages, opt => opt.Ignore());
            CreateMap<ShipmentCreateDTO, Shipment>();
        }
    }
}
