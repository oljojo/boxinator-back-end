﻿namespace Boxinator.DTOs.StatusDTO
{
    public class StatusCreateDTO
    {
        public string? Name { get; set; }    
        public List<int>? ShipmentIds { get; set; }
    }
}
