﻿namespace Boxinator.DTOs.UserDTO
{
    public class UserReadDTO
    {
        public int Id { get; set; }
        public string? KeyCloakId { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Email { get; set; }
        public string? DateOfBirth { get; set; }
        public string? Country { get; set; }
        public string? ZipCode { get; set; }
        public string? PhoneNumber { get; set; }
        public List<int>? ShipmentIds { get; set; }
    }
}
