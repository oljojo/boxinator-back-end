﻿using System.ComponentModel.DataAnnotations;

namespace Boxinator.DTOs.UerDTO
{
    public class UserEditDTO
    {
        public int Id { get; set; }

        [MaxLength(60)]
        public string? KeyCloakId { get; set; }

        [Required]
        [MaxLength(60)]
        public string? FirstName { get; set; }
        [MaxLength(60)]
        public string? LastName { get; set; }
        [MaxLength(100)]
        public string? Email { get; set; }
        public string? DateOfBirth { get; set; }
        [MaxLength(60)]
        public string? Country { get; set; }
        [MaxLength(60)]
        public string? ZipCode { get; set; }
        [MaxLength(60)]
        public string? PhoneNumber { get; set; }
        public List<int>? ShipmentIds { get; set; }
    }
}
