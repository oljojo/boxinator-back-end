﻿using Boxinator.DTOs.TierDTO;

namespace Boxinator.DTOs.PackageDTO
{
    public class PackageReadDTO
    {
        public int Id { get; set; }
        public TierReadDTO? Tier { get; set; }
        public int? ShipmentId { get; set; }
        public double? Weight { get; set; }
        public string? Color { get; set; }
    }
}
