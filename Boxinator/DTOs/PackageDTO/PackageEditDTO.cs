﻿using System.ComponentModel.DataAnnotations;

namespace Boxinator.DTOs.PackageDTO
{
    public class PackageEditDTO
    {
        public int? Id { get; set; }
        public int? TierId { get; set; }
        public double? Weight { get; set; }
        [MaxLength(60)]
        public string? Color { get; set; }
    }
}
