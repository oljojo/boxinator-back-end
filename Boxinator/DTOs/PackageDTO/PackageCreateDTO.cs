﻿using Boxinator.DTOs.TierDTO;
using System.ComponentModel.DataAnnotations;

namespace Boxinator.DTOs.PackageDTO
{
    public class PackageCreateDTO
    {
        public int? TierId { get; set; }
        public double? Weight { get; set; }
        [MaxLength(60)]
        public string? Color { get; set; }
    }
}
