﻿using System.ComponentModel.DataAnnotations;

namespace Boxinator.DTOs.TierDTO
{
    public class TierCreateDTO
    {
        [MaxLength(60)]
        public string? Name { get; set; }
        public double? maxWeight { get; set; }
        public double? minWeight { get; set; }
    }
}
