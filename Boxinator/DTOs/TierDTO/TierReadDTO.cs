﻿namespace Boxinator.DTOs.TierDTO
{
    public class TierReadDTO
    {
        public int Id { get; set; }
        public string? name { get; set; }
    }
}
