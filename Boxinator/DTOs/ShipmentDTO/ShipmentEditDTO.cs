﻿using Boxinator.DTOs.PackageDTO;
using System.ComponentModel.DataAnnotations;

namespace Boxinator.DTOs.ShipmentDTO
{
    public class ShipmentEditDTO
    {
        public int? UserId { get; set; }
        public int? CountryId { get; set; }
        [MaxLength(60)]
        public string? receiverName { get; set; }
        public double? TotalCost { get; set; }
        public double? TotalWeight { get; set; }
        public List<int>? StatusIds { get; set; }
        public List<PackageEditDTO>? Packages { get; set; }
    }
}
