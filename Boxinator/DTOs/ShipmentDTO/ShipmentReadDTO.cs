﻿using Boxinator.DTOs.PackageDTO;
using Boxinator.DTOs.StatusDTO;
using Boxinator.Entity.Models;

namespace Boxinator.DTOs.ShipmentDTO
{
    public class ShipmentReadDTO
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public int? CountryId { get; set; }
        public string? receiverName { get; set; }
        public double? TotalCost { get; set; }
        public double? TotalWeight { get; set; }
        public List<StatusReadDTO>? Status { get; set; }
        public List<PackageReadDTO>? Packages { get; set; }
    }
}
