﻿namespace Boxinator.DTOs.CountryDTO
{
    public class CountryReadDTO
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public int? Rate { get; set; }
    }
}
