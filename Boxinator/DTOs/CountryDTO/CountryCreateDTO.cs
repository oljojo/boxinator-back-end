﻿namespace Boxinator.DTOs.CountryDTO
{
    public class CountryCreateDTO
    {
        public string? Name { get; set; }
        public int? Rate { get; set; }
    }
}
