using Boxinator.Entity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using System.Reflection;


var builder = WebApplication.CreateBuilder(args);

// Add services to the container.




builder.Services.AddAutoMapper(typeof(Program));


builder.Services.AddCors(option =>
{
    option.AddPolicy("CorsPolicy",
        policy =>
        {

            policy.AllowAnyOrigin()
            .AllowAnyHeader()
            .AllowAnyMethod();
        });
});


builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    options.SwaggerDoc("v1", new OpenApiInfo
    {
        Version = "v1",
        Title = "Boxinator API",
        Description = "An ASP.NET Core Web API for managing Boxinator. All end-points are protected and require a Bearer token from Keycloak to access.",
        Contact = new OpenApiContact
        {
            Name = "Project Boxinator",
            Url = new Uri("https://boxinator-case.herokuapp.com/home")
        }
    });
    var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));
});

builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
        .AddJwtBearer(options =>
        {
            options.TokenValidationParameters = new TokenValidationParameters
            {
                //Access token for postman can be found at http://localhost:8000/#
                //requires token from keycloak instance - location stored in secret manager
                IssuerSigningKeyResolver = (token, securityToken, kid, parameters) =>
                {
                    var client = new HttpClient();
                    string keyuri = "https://keycloak-g.herokuapp.com/auth/realms/Boxinator/protocol/openid-connect/certs";
                    //Retrieves the keys from keycloak instance to verify token
                    var response = client.GetAsync(keyuri).Result;
                    var responseString = response.Content.ReadAsStringAsync().Result;
                    var keys = JsonConvert.DeserializeObject<JsonWebKeySet>(responseString);
                    return keys.Keys;
                },

                ValidIssuers = new List<string>
                   {
                        "https://keycloak-g.herokuapp.com/auth/realms/Boxinator"
                   },

                //This checks the token for a the 'aud' claim value
                ValidAudience = "account",

            };

        });

builder.Configuration.AddEnvironmentVariables();

if (builder.Environment.IsProduction())
{
    var connectionString = Environment.GetEnvironmentVariable("ConnectionString");

    builder.Services.AddDbContext<BoxinatorDbContext, BoxinatorPublicDbContext>(option => option.UseNpgsql(connectionString));
}
else
{
    var connectionString = builder.Configuration.GetConnectionString("Local");

    builder.Services.AddDbContext<BoxinatorDbContext>(option => option.UseSqlServer(connectionString));
}




var app = builder.Build();

// Configure the HTTP request pipeline.


app.UseCors("CorsPolicy");

app.UseSwagger();
app.UseSwaggerUI();

app.UseHttpsRedirection();

app.UseAuthentication();

app.UseAuthorization();


app.MapControllers();
if (app.Environment.IsProduction())
{
    using (var scope = app.Services.CreateScope())
    {
        var services = scope.ServiceProvider;

        var context = services.GetRequiredService<BoxinatorPublicDbContext>();
        if (context.Database.GetPendingMigrations().Any())
        {
            context.Database.Migrate();
        }
    }
}


app.Run();
