﻿using Microsoft.EntityFrameworkCore;

namespace Boxinator.Entity
{
    public class BoxinatorPublicDbContext : BoxinatorDbContext
    {
        public BoxinatorPublicDbContext(DbContextOptions<BoxinatorPublicDbContext> options) : base(options)
        {
        }

    }
}
