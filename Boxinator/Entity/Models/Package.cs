﻿using System.ComponentModel.DataAnnotations;

namespace Boxinator.Entity.Models
{
    public class Package
    {
        public int Id { get; set; }
        public int? TierId { get; set; }
        public Tier? Tier { get; set; }
        public int? ShipmentId { get; set; }
        public Shipment? Shipment { get; set; }
        public double? Weight { get; set; }
        [MaxLength(60)]
        public string? Color { get; set; }

    }
}
