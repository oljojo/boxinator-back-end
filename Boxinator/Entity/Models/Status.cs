﻿using System.ComponentModel.DataAnnotations;

namespace Boxinator.Entity.Models
{
    public class Status
    {
        public int Id { get; set; }
        [MaxLength(60)]
        public string? Name { get; set; }
        public ICollection<Shipment>? Shipment { get; set; }
    }
}
