﻿using System.ComponentModel.DataAnnotations;

namespace Boxinator.Entity.Models
{
    public class Country
    {
        public int Id { get; set; }
        [MaxLength(60)]
        public string? Name { get; set; }
        public int? Rate { get; set; }
        public ICollection<Shipment>? Shipments { get; set; }
    }
}
