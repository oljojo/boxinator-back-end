﻿using System.ComponentModel.DataAnnotations;

namespace Boxinator.Entity.Models
{
    public class User
    {
        public int Id { get; set; }

        [MaxLength(60)]
        public string? KeyCloakId { get; set; }
        public string? FirstName { get; set; }
        [MaxLength(60)]
        public string? LastName { get; set; }
        [MaxLength(100)]
        public string? Email { get; set; }
        [MaxLength(60)]
        public string? DateOfBirth { get; set; }
        [MaxLength(60)]
        public string? Country { get; set; }
        [MaxLength(60)]
        public string? ZipCode { get; set; }
        [MaxLength(60)]
        public string? PhoneNumber { get; set; }

        public ICollection<Shipment>? Shipments { get; set; }

    }
}

