﻿using System.ComponentModel.DataAnnotations;

namespace Boxinator.Entity.Models
{
    public class Shipment
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public User? User { get; set; }
        public int? CountryId { get; set; }
        public Country? Country { get; set; }
        [MaxLength(60)]
        public string? receiverName { get; set; }
        public double? TotalCost { get; set; }
        public double? TotalWeight { get; set; }
        public ICollection<Status>? Status { get; set; }
        public ICollection<Package>? Packages { get; set; }


    }
}
