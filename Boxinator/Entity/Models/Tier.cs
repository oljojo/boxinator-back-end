﻿using System.ComponentModel.DataAnnotations;

namespace Boxinator.Entity.Models
{
    public class Tier
    {
        public int Id{ get; set; }
        [MaxLength(60)]
        public string? name { get; set; }
        public double? minWeight { get; set; }
        public double? maxWeight { get; set; }
    }
}
