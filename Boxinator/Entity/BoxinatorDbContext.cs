﻿using Boxinator.Entity.Models;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace Boxinator.Entity
{
    public class BoxinatorDbContext : DbContext
    {
        public BoxinatorDbContext(DbContextOptions<BoxinatorDbContext> options) : base(options)
        {
        }
        protected BoxinatorDbContext(DbContextOptions options)
        : base(options)
        {
        }

        public DbSet<User> User { get; set; }
        public DbSet<Status> Status { get; set; }

        public DbSet<Country> Country { get; set; }

        public DbSet<Package> Package { get; set; }

        public DbSet<Shipment> Shipment { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasData(new User
                {
                    Id = 1,
                    KeyCloakId = "testkey",
                    FirstName = "Olof",
                    LastName = "Johnsson",
                    Email = "olof@olof.olof",
                    DateOfBirth = "95/12/30",
                    Country = "Germany",
                    ZipCode = "12398",
                    PhoneNumber = "0702345555"

                });
            modelBuilder.Entity<User>()
                .HasData(new User
                {
                    Id = 2,
                    KeyCloakId = null,
                    FirstName = "Jeremy",
                    LastName = "Matthiessen",
                    Email = "jeremy@jeremy.jeremy",
                    DateOfBirth = "99/02/05",
                    Country = "Norway",
                    ZipCode = "67812",
                    PhoneNumber = "07023578555"
                });
            modelBuilder.Entity<User>()
                .HasData(new User
                {
                    Id = 3,
                    KeyCloakId = null,
                    FirstName = "Usko",
                    LastName = "Usko",
                    Email = "usko@usko.usko",
                    DateOfBirth = "97/06/15",
                    Country = "Sweden",
                    ZipCode = "43587",
                    PhoneNumber = "07025673555"
                });
            modelBuilder.Entity<Country>()
                .HasData(new Country
                {
                    Id = 1,
                    Name = "Sweden",
                    Rate = 0
                });
            modelBuilder.Entity<Country>()
                .HasData(new Country
                {
                    Id = 2,
                    Name = "Spain",
                    Rate = 25
                });
            modelBuilder.Entity<Country>()
                .HasData(new Country
                {
                    Id = 3,
                    Name = "USA",
                    Rate = 50
                });
            modelBuilder.Entity<Tier>()
                .HasData(new Tier
                {
                    Id = 1,
                    name = "Basic",
                    minWeight = 0,
                    maxWeight = 1

                });
            modelBuilder.Entity<Tier>()
                .HasData(new Tier
                {
                    Id = 2,
                    name = "Humble",
                    minWeight = 1,
                    maxWeight = 2

                });
            modelBuilder.Entity<Tier>()
                .HasData(new Tier
                {
                    Id = 3,
                    name = "Deluxe",
                    minWeight = 2,
                    maxWeight = 5

                });
            modelBuilder.Entity<Tier>()
                .HasData(new Tier
                {
                    Id = 4,
                    name = "Premium",
                    minWeight = 5,
                    maxWeight = 8

                });
            modelBuilder.Entity<Status>()
                .HasData(new Status
                {
                    Id = 1,
                    Name = "CREATED"

                });
            modelBuilder.Entity<Status>()
                .HasData(new Status
                {
                    Id = 2,
                    Name = "RECIEVED"

                });
            modelBuilder.Entity<Status>()
                .HasData(new Status
                {
                    Id = 3,
                    Name = "INTRANSIT"

                });
            modelBuilder.Entity<Status>()
                .HasData(new Status
                {
                    Id = 4,
                    Name = "COMPLETED"

                });
            modelBuilder.Entity<Status>()
                .HasData(new Status
                {
                    Id = 5,
                    Name = "CANCELLED"

                });
            modelBuilder.Entity<Shipment>()
                .HasData(new Shipment
                {
                    Id = 1,
                    UserId = 1,
                    CountryId = 1,
                    receiverName = "Bertil",
                    TotalCost = null,
                    TotalWeight = null
                });
            modelBuilder.Entity<Shipment>()
                .HasData(new Shipment
                {
                    Id = 2,
                    UserId = 3,
                    CountryId = 2,
                    receiverName = "Göran",
                    TotalCost = null,
                    TotalWeight = null
                });
            modelBuilder.Entity<Shipment>()
                .HasData(new Shipment
                {
                    Id = 3,
                    UserId = 2,
                    CountryId = 3,
                    receiverName = "Sven",
                    TotalCost = null,
                    TotalWeight = null
                });


            modelBuilder.Entity<Shipment>()
                .HasMany(s => s.Status)
                .WithMany(st => st.Shipment)
                .UsingEntity<Dictionary<string, object>>(
                    "ShipmentStatus",
                    r => r.HasOne<Status>().WithMany().HasForeignKey("StatusId"),
                    l => l.HasOne<Shipment>().WithMany().HasForeignKey("ShipmentId"),
                    je =>
                    {
                        je.HasKey("StatusId", "ShipmentId");
                        je.HasData(
                            new { StatusId = 1, ShipmentId = 1 },
                            new { StatusId = 1, ShipmentId = 2 },
                            new { StatusId = 1, ShipmentId = 3 },
                            new { StatusId = 2, ShipmentId = 1 },
                            new { StatusId = 2, ShipmentId = 2 },
                            new { StatusId = 2, ShipmentId = 3 },
                            new { StatusId = 3, ShipmentId = 1 }
                        );
                    });



            modelBuilder.Entity<Package>()
                .HasData(new Package
                {
                    Id = 1,
                    TierId = 1,
                    ShipmentId = 1,
                    Weight = 0.5,
                    Color = "Blue"

                });
            modelBuilder.Entity<Package>()
                .HasData(new Package
                {
                    Id = 2,
                    TierId = 1,
                    ShipmentId = 1,
                    Weight = 0.3,
                    Color = "Green"

                });
            modelBuilder.Entity<Package>()
                .HasData(new Package
                {
                    Id = 3,
                    TierId = 1,
                    ShipmentId = 1,
                    Weight = 0.7,
                    Color = "Red"

                });
            modelBuilder.Entity<Package>()
                .HasData(new Package
                {
                    Id = 4,
                    TierId = 1,
                    ShipmentId = 2,
                    Weight = 0.5,
                    Color = "Blue"

                });
            modelBuilder.Entity<Package>()
                .HasData(new Package
                {
                    Id = 5,
                    TierId = 1,
                    ShipmentId = 2,
                    Weight = 0.3,
                    Color = "Green"

                });
            modelBuilder.Entity<Package>()
                .HasData(new Package
                {
                    Id = 6,
                    TierId = 1,
                    ShipmentId = 2,
                    Weight = 0.7,
                    Color = "Red"

                });
            modelBuilder.Entity<Package>()
                .HasData(new Package
                {
                    Id = 7,
                    TierId = 2,
                    ShipmentId = 3,
                    Weight = 1.5,
                    Color = "Blue"

                });
            modelBuilder.Entity<Package>()
                .HasData(new Package
                {
                    Id = 8,
                    TierId = 3,
                    ShipmentId = 3,
                    Weight = 2.3,
                    Color = "Green"

                });
            modelBuilder.Entity<Package>()
                .HasData(new Package
                {
                    Id = 9,
                    TierId = 4,
                    ShipmentId = 3,
                    Weight = 5.7,
                    Color = "Red"

                });
        }
    }
}
