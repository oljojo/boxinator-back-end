﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Boxinator.Entity;
using Boxinator.Entity.Models;
using Boxinator.DTOs.CountryDTO;
using AutoMapper;
using System.Net.Mime;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Authorization;

namespace Boxinator.Controllers
{
    [EnableCors("CorsPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class CountriesController : ControllerBase
    {
        private readonly BoxinatorDbContext _context;
        private readonly IMapper _mapper;
        public CountriesController(BoxinatorDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all countries
        /// </summary>
        /// <returns>A list of countries and a status code.</returns>
        [Authorize]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CountryReadDTO>>> GetCountry()
        {
          if (_context.Country == null)
          {
              return NotFound();
          }
            return _mapper.Map<List<CountryReadDTO>>(await _context.Country.ToListAsync());
        }

        /// <summary>
        /// Gets a specific country by the given id.
        /// </summary>
        /// <param name="id">The id of the country.</param>
        /// <returns>A country or a status code.</returns>
        [Authorize]
        [HttpGet("{id}")]
        public async Task<ActionResult<CountryReadDTO>> GetCountry(int id)
        {
          if (_context.Country == null)
          {
              return NotFound();
          }
            var country = await _context.Country.FindAsync(id);

            if (country == null)
            {
                return NotFound();
            }

            return _mapper.Map<CountryReadDTO>(country);
        }

        /// <summary>
        /// Edits a movie with the data specified in the body.
        /// </summary>
        /// <param name="id">The id of the country.</param>
        /// <param name="country">The country.</param>
        /// <returns>A status code.</returns>
        [Authorize]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCountry(int id, CountryEditDTO country)
        {
            if (id != country.Id)
            {
                return BadRequest();
            }

            Country updatedCountry = await _context.Country.FindAsync(id);
            _mapper.Map<CountryEditDTO, Country>(country, updatedCountry);
            _context.Entry(updatedCountry).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CountryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Creates a new country with the data provided in the body.
        /// </summary>
        /// <param name="country">The country data.</param>
        /// <returns>A status code.</returns>
        [Authorize]
        [HttpPost]
        public async Task<ActionResult<CountryCreateDTO>> PostCountry(CountryCreateDTO country)
        {
            if (_context.Country == null)
            {
                return Problem("Entity set 'BoxinatorDbContext.Country'  is null.");
            }

            Country domainCountry = _mapper.Map<Country>(country);

            _context.Country.Add(domainCountry);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCountry", new { id = domainCountry.Id }, country);
        }

        private bool CountryExists(int id)
        {
            return (_context.Country?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
