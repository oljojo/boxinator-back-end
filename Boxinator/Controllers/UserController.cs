﻿using Boxinator.Entity;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Boxinator.DTOs.UserDTO;
using Microsoft.EntityFrameworkCore;
using Boxinator.Entity.Models;
using Boxinator.DTOs.UerDTO;
using System.Net.Mime;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;

namespace Boxinator.Controllers
{
    [EnableCors("CorsPolicy")]
    [Route("api/v1/users")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]

    public class UserController : ControllerBase
    {
        private readonly BoxinatorDbContext _boxinatorDbContext;
        private readonly IMapper _mapper;

        public UserController(BoxinatorDbContext boxinatorDbContext, IMapper mapper)
        {
            _boxinatorDbContext = boxinatorDbContext;
            _mapper = mapper;
        }
        /// <summary>
        /// gets all users with get request
        /// </summary>
        /// <returns></returns>

        [Authorize]
        [HttpGet("getallUsers")]
        public async Task<ActionResult<IEnumerable<UserReadDTO>>> getAllUsers()
        {
            var readUserDto = _mapper.Map<List<UserReadDTO>>(await _boxinatorDbContext.User.Include(c => c.Shipments).ToListAsync());

            return Ok(readUserDto);
        }


        /// <summary>
        /// gets A user by Id with get request
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        [Authorize]
        [HttpGet("getUserbyId")]
        public async Task<ActionResult<IEnumerable<UserReadDTO>>> GetUserById( int id)
        {
            var users = await _boxinatorDbContext.User.Select(u => u).Where(u => u.Id == id).Include(u => u.Shipments).FirstAsync();

            if (users == null)
            {
                return NotFound();
            }

            var userDto = _mapper.Map<UserReadDTO>(users);

            return Ok(userDto);

        }

        /// <summary>
        /// Gets a user by keycloakId
        /// </summary>
        /// <param name="keyloakId"></param>
        /// <returns></returns>

        [Authorize]
        [HttpGet("getUserbykeyCloakId")]
        public async Task<ActionResult<IEnumerable<UserReadDTO>>> GetUserBykeyCloakId(string keyloakId)
        {
            var users = await _boxinatorDbContext.User.Select(u => u).Where(u => u.KeyCloakId == keyloakId).Include(u => u.Shipments).FirstAsync();

            if (users == null)
            {
                return NotFound();
            }

            var userDto = _mapper.Map<UserReadDTO>(users);

            return Ok(userDto);

        }
        /// <summary>
        /// registers a user by creating one
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>


        [Authorize]
        [HttpPost("Register")]
        public async Task<ActionResult<IEnumerable<UserCreateDTO>>> Register(UserCreateDTO user)
        {
            var domainUser = _mapper.Map<User>(user);
            if(user.ShipmentIds != null) 
                domainUser.Shipments = (from m in _boxinatorDbContext.Shipment where user.ShipmentIds.Contains(m.Id) select m).ToList();
            await _boxinatorDbContext.User.AddAsync(domainUser);
            await _boxinatorDbContext.SaveChangesAsync();

            return CreatedAtAction("GetUserById", new
            {
                userId = domainUser.Id,
            }, user);

        }

        /// <summary>
        /// update a character with id with put request
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="updatedUser"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPut("{userId}")]
        public async Task<ActionResult> UpdateUserById(int userId, UserEditDTO updatedUser )
        {
            if(userId != updatedUser.Id)
            {
                return BadRequest();
            }

            User user = _boxinatorDbContext.User.Include("Shipments").Single(f => f.Id == updatedUser.Id);
            if (updatedUser.ShipmentIds != null)
            {
                List<Shipment> shipments = new List<Shipment>();

                foreach (var id in updatedUser.ShipmentIds)
                {
                    var shipment = await _boxinatorDbContext.Shipment.FindAsync(id);

                    if (shipment != null)
                    {
                        shipments.Add(shipment);
                    }

                    else
                    {
                        return BadRequest();
                    }
                }
                user.Shipments = shipments;
            }
            else
            {
                List<Shipment> shipments = null;
                user.Shipments = shipments;
            }
            user.KeyCloakId = updatedUser.KeyCloakId;
            user.FirstName = updatedUser.FirstName;
            user.LastName = updatedUser.LastName;
            user.Email = updatedUser.Email;
            user.DateOfBirth = updatedUser.DateOfBirth;
            user.Country = updatedUser.Country;
            user.ZipCode = updatedUser.ZipCode;
            user.PhoneNumber = updatedUser.PhoneNumber;
            await _boxinatorDbContext.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// deletes a user by id
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpDelete("DeleteUser{userId}")]
        public async Task<ActionResult> DeleteUser(int userId)
        {
            var user = await _boxinatorDbContext.User.FindAsync(userId);
            if(user == null)
            {
                return NotFound();
            }
            try
            {
                _boxinatorDbContext.User.Remove(user);
                await _boxinatorDbContext.SaveChangesAsync();

            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return NoContent();

        }


    }
}
