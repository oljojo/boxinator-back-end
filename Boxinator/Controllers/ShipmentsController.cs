﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Boxinator.Entity;
using Boxinator.Entity.Models;
using Boxinator.DTOs.ShipmentDTO;
using AutoMapper;
using System.Net.Mime;
using Boxinator.DTOs.PackageDTO;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Authorization;

namespace Boxinator.Controllers
{
    [EnableCors("CorsPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class ShipmentsController : ControllerBase
    {
        private readonly BoxinatorDbContext _context;
        private readonly IMapper _mapper;

        public ShipmentsController(BoxinatorDbContext context, IMapper  mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all shipments.
        /// </summary>
        /// <returns>A list of shipments or "Not found"</returns>
        [Authorize]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ShipmentReadDTO>>> GetShipment()
        {
            if (_context.Shipment == null)
            {
                return Problem("Database Error");
            }
            var shipmentList = await _context.Shipment
                .Include(s => s.Packages)
                    .ThenInclude(p => p.Tier)
                .Include(s => s.Status).ToListAsync();
            if(shipmentList.Count > 0)
            {
                return _mapper.Map<List<ShipmentReadDTO>>(shipmentList);
            }
            else
            {
                return NotFound("No shipments in database");
            }
            
        }


        /// <summary>
        /// Gets all completed shipments.
        /// </summary>
        /// <returns>A list of shipments or "Not found"</returns>
        [Authorize]
        [HttpGet("completed")]
        public async Task<ActionResult<IEnumerable<ShipmentReadDTO>>> GetCompletedShipment()
        {
            if (_context.Shipment == null)
            {
                return Problem("Database Error");
            }

            var status = await _context.Status.FirstOrDefaultAsync(status => status.Name == "COMPLETED");

            var shipmentList = await _context.Shipment
                .Include(s => s.Packages)
                    .ThenInclude(p => p.Tier)
                .Include(s => s.Status)
                .Select(shipment => shipment).Where(shipment => shipment.Status.Contains(status)).ToListAsync();


            if (shipmentList.Count > 0)
            {
                return _mapper.Map<List<ShipmentReadDTO>>(shipmentList);
            }
            else
            {
                return NotFound("No shipments in database");
            }

        }



        /// <summary>
        /// Gets all cancelled shipments.
        /// </summary>
        /// <returns>A list of shipments or "Not found"</returns>
        [Authorize]
        [HttpGet("cancelled")]
        public async Task<ActionResult<IEnumerable<ShipmentReadDTO>>> GetCancelledShipment()
        {
            if (_context.Shipment == null)
            {
                return Problem("Database Error");
            }

            var status = await _context.Status.FirstOrDefaultAsync(status => status.Name == "CANCELLED");

            var shipmentList = await _context.Shipment
                .Include(s => s.Packages)
                    .ThenInclude(p => p.Tier)
                .Include(s => s.Status)
                .Select(shipment => shipment).Where(shipment => shipment.Status.Contains(status)).ToListAsync();


            if (shipmentList.Count > 0)
            {
                return _mapper.Map<List<ShipmentReadDTO>>(shipmentList);
            }
            else
            {
                return NotFound("No shipments in database");
            }

        }


        /// <summary>
        /// Gets all shipments based on a customer/user.
        /// </summary>
        /// <returns>A list of shipments or "Not found"</returns>
        [Authorize]
        [HttpGet("user/{id}")]
        public async Task<ActionResult<IEnumerable<ShipmentReadDTO>>> GetShipmentByUserId(int id)
        {
            if (_context.Shipment == null)
            {
                return Problem("Database Error");
            }

            var shipmentList = await _context.Shipment
                .Include(s => s.Packages)
                    .ThenInclude(p => p.Tier)
                .Include(s => s.Status)
                .Select(shipment => shipment).Where(shipment => shipment.UserId == id).ToListAsync();


            if (shipmentList.Count > 0)
            {
                return _mapper.Map<List<ShipmentReadDTO>>(shipmentList);
            }
            else
            {
                return NotFound("No shipments in database");
            }

        }


        /// <summary>
        /// Gets a shipment based on its id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A shipment or "Not found"</returns>
        [Authorize]
        [HttpGet("{id}")]
        public async Task<ActionResult<ShipmentReadDTO>> GetShipment(int id)
        {
            if (_context.Shipment == null)
            {
                return Problem("Database Error");
            }
            if (!ShipmentExists(id))
            {
                return NotFound($"Shipment with id: {id} doesn't exist in database");
            }
            var shipment = await _context.Shipment.Include(s => s.Packages)
                    .ThenInclude(p => p.Tier)
                .Include(s => s.Status).FirstOrDefaultAsync(s => s.Id == id);

            return _mapper.Map<ShipmentReadDTO>(shipment);
        }

        /// <summary>
        /// Edits a shipment based on its id.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="editShipment"></param>
        /// <returns>The edited shipment or "Not found"</returns>
        [Authorize]
        [HttpPut("{id}")]
        public async Task<ActionResult<ShipmentReadDTO>> PutShipment(int id, ShipmentEditDTO editShipment)
        {
            Shipment shipment = await _context.Shipment
                .Include(s => s.Status)
                .Include(s => s.Packages)
                    .ThenInclude(p => p.Tier)
                .FirstOrDefaultAsync(sh => sh.Id == id);


            _mapper.Map<ShipmentEditDTO, Shipment>(editShipment, shipment);
            if (editShipment.StatusIds != null)
            {
                shipment.Status = await _context.Status.Select(s => s).Where(st => editShipment.StatusIds.Contains(st.Id)).ToListAsync();
            }
            List<Package> packages = new List<Package>();
            foreach (PackageEditDTO packageDto in editShipment.Packages)
            {
                Package _package = await _context.Package.FindAsync(packageDto.Id);
                if (_package != null)
                {
                    _mapper.Map<PackageEditDTO, Package>(packageDto, _package);
                    _package.ShipmentId = id;
                    packages.Add(_package);
                }
                else 
                {
                    _package = new();
                    _package = _mapper.Map<Package>(packageDto);
                    _context.Package.Add(_package);
                    _package.ShipmentId = id;
                    packages.Add(_package);
                }
                

            }
            shipment.Packages = packages;
            _context.Entry(shipment).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ShipmentExists(id))
                {
                    return NotFound($"Shipment with id: {id} doesn't exist in database");
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetShipment", new { id = shipment.Id }, editShipment);
        }


        /// <summary>
        /// Creates a shipment and adds it to the database.
        /// </summary>
        /// <param name="createShipment"></param>
        /// <returns>The created shipment or "Not found"</returns>
        /// 
        [Authorize]
        [HttpPost]
        public async Task<ActionResult<ShipmentReadDTO>> PostShipment(ShipmentCreateDTO createShipment)
        {
            if (_context.Shipment == null)
            {
                return Problem("Database Error");
            }

            Shipment shipment = new();
            shipment = _mapper.Map<Shipment>(createShipment);
            if (createShipment.StatusIds != null)
            {
                shipment.Status = await _context.Status.Select(s => s).Where(st => createShipment.StatusIds.Contains(st.Id)).ToListAsync(); ;
            }
            foreach (Package package in shipment.Packages)
            {
                _context.Package.Add(package);
            }
            _context.Shipment.Add(shipment);
            await _context.SaveChangesAsync();

            //return NoContent();
            return CreatedAtAction("GetShipment", new { id = shipment.Id }, createShipment);
        }

        /// <summary>
        /// Deletes a shipment based on its id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>The deleted shipment or "Not found"</returns>
        [Authorize]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteShipment(int id)
        {
            if (_context.Shipment == null)
            {
                return Problem("Database Error");
            }

            Shipment shipment = await _context.Shipment
                .Include(s => s.Status)
                .Include(s => s.Packages)
                    .ThenInclude(p => p.Tier)
                .FirstOrDefaultAsync(sh => sh.Id == id);

            if (shipment == null)
            {
                return NotFound($"Shipment with id: {id} doesn't exist in database");
            }
            //remove packages in the shipment
            foreach(var p in shipment.Packages)
            {
                _context.Package.Remove(p);
            }

            _context.Shipment.Remove(shipment);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool ShipmentExists(int id)
        {
            return (_context.Shipment?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
