﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Boxinator.Migrations
{
    public partial class seedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_ShipmentStatus",
                table: "ShipmentStatus");

            migrationBuilder.DropIndex(
                name: "IX_ShipmentStatus_StatusId",
                table: "ShipmentStatus");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ShipmentStatus",
                table: "ShipmentStatus",
                columns: new[] { "StatusId", "ShipmentId" });

            migrationBuilder.InsertData(
                table: "Country",
                columns: new[] { "Id", "Name", "Rate" },
                values: new object[,]
                {
                    { 1, "Sweden", 0 },
                    { 2, "Spain", 25 },
                    { 3, "USA", 50 }
                });

            migrationBuilder.InsertData(
                table: "Status",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "CREATED" },
                    { 2, "RECIEVED" },
                    { 3, "INTRANSIT" },
                    { 4, "COMPLETED" },
                    { 5, "CANCELLED" }
                });

            migrationBuilder.InsertData(
                table: "Tier",
                columns: new[] { "Id", "maxWeight", "minWeight", "name" },
                values: new object[,]
                {
                    { 1, 1.0, 0.0, "Basic" },
                    { 2, 2.0, 1.0, "Humble" },
                    { 3, 5.0, 2.0, "Deluxe" },
                    { 4, 8.0, 5.0, "Premium" }
                });

            migrationBuilder.InsertData(
                table: "User",
                columns: new[] { "Id", "Country", "DateOfBirth", "Email", "FirstName", "LastName", "PhoneNumber", "ZipCode" },
                values: new object[,]
                {
                    { 1, "Germany", "95/12/30", "olof@olof.olof", "Olof", "Johnsson", "0702345555", "12398" },
                    { 2, "Norway", "99/02/05", "jeremy@jeremy.jeremy", "Jeremy", "Matthiessen", "07023578555", "67812" },
                    { 3, "Sweden", "97/06/15", "usko@usko.usko", "Usko", "Usko", "07025673555", "43587" }
                });

            migrationBuilder.InsertData(
                table: "Shipment",
                columns: new[] { "Id", "CountryId", "TotalCost", "TotalWeight", "UserId", "receiverName" },
                values: new object[] { 1, 1, null, null, 1, "Bertil" });

            migrationBuilder.InsertData(
                table: "Shipment",
                columns: new[] { "Id", "CountryId", "TotalCost", "TotalWeight", "UserId", "receiverName" },
                values: new object[] { 2, 2, null, null, 3, "Göran" });

            migrationBuilder.InsertData(
                table: "Shipment",
                columns: new[] { "Id", "CountryId", "TotalCost", "TotalWeight", "UserId", "receiverName" },
                values: new object[] { 3, 3, null, null, 2, "Sven" });

            migrationBuilder.InsertData(
                table: "Package",
                columns: new[] { "Id", "Color", "ShipmentId", "TierId", "Weight", "receiverName" },
                values: new object[,]
                {
                    { 1, "Blue", 1, 1, 0.5, null },
                    { 2, "Green", 1, 1, 0.29999999999999999, null },
                    { 3, "Red", 1, 1, 0.69999999999999996, null },
                    { 4, "Blue", 2, 1, 0.5, null },
                    { 5, "Green", 2, 1, 0.29999999999999999, null },
                    { 6, "Red", 2, 1, 0.69999999999999996, null },
                    { 7, "Blue", 3, 2, 1.5, null },
                    { 8, "Green", 3, 3, 2.2999999999999998, null },
                    { 9, "Red", 3, 4, 5.7000000000000002, null }
                });

            migrationBuilder.InsertData(
                table: "ShipmentStatus",
                columns: new[] { "ShipmentId", "StatusId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 2, 1 },
                    { 3, 1 },
                    { 1, 2 },
                    { 2, 2 },
                    { 3, 2 },
                    { 1, 3 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_ShipmentStatus_ShipmentId",
                table: "ShipmentStatus",
                column: "ShipmentId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_ShipmentStatus",
                table: "ShipmentStatus");

            migrationBuilder.DropIndex(
                name: "IX_ShipmentStatus_ShipmentId",
                table: "ShipmentStatus");

            migrationBuilder.DeleteData(
                table: "Package",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Package",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Package",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Package",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Package",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Package",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Package",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Package",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Package",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "ShipmentStatus",
                keyColumns: new[] { "ShipmentId", "StatusId" },
                keyValues: new object[] { 1, 1 });

            migrationBuilder.DeleteData(
                table: "ShipmentStatus",
                keyColumns: new[] { "ShipmentId", "StatusId" },
                keyValues: new object[] { 2, 1 });

            migrationBuilder.DeleteData(
                table: "ShipmentStatus",
                keyColumns: new[] { "ShipmentId", "StatusId" },
                keyValues: new object[] { 3, 1 });

            migrationBuilder.DeleteData(
                table: "ShipmentStatus",
                keyColumns: new[] { "ShipmentId", "StatusId" },
                keyValues: new object[] { 1, 2 });

            migrationBuilder.DeleteData(
                table: "ShipmentStatus",
                keyColumns: new[] { "ShipmentId", "StatusId" },
                keyValues: new object[] { 2, 2 });

            migrationBuilder.DeleteData(
                table: "ShipmentStatus",
                keyColumns: new[] { "ShipmentId", "StatusId" },
                keyValues: new object[] { 3, 2 });

            migrationBuilder.DeleteData(
                table: "ShipmentStatus",
                keyColumns: new[] { "ShipmentId", "StatusId" },
                keyValues: new object[] { 1, 3 });

            migrationBuilder.DeleteData(
                table: "Status",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Status",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Shipment",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Shipment",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Shipment",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Status",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Status",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Status",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Tier",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Tier",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Tier",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Tier",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Country",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Country",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Country",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "User",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "User",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "User",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.AddPrimaryKey(
                name: "PK_ShipmentStatus",
                table: "ShipmentStatus",
                columns: new[] { "ShipmentId", "StatusId" });

            migrationBuilder.CreateIndex(
                name: "IX_ShipmentStatus_StatusId",
                table: "ShipmentStatus",
                column: "StatusId");
        }
    }
}
