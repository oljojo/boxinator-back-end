﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Boxinator.Migrations
{
    public partial class shipment_user_changes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ShipmentStatus_Shipment_ShipmentsId",
                table: "ShipmentStatus");

            migrationBuilder.DropForeignKey(
                name: "FK_ShipmentStatus_Status_StatusesId",
                table: "ShipmentStatus");

            migrationBuilder.RenameColumn(
                name: "StatusesId",
                table: "ShipmentStatus",
                newName: "StatusId");

            migrationBuilder.RenameColumn(
                name: "ShipmentsId",
                table: "ShipmentStatus",
                newName: "ShipmentId");

            migrationBuilder.RenameIndex(
                name: "IX_ShipmentStatus_StatusesId",
                table: "ShipmentStatus",
                newName: "IX_ShipmentStatus_StatusId");

            migrationBuilder.AddColumn<string>(
                name: "Country",
                table: "User",
                type: "nvarchar(60)",
                maxLength: 60,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "DateOfBirth",
                table: "User",
                type: "nvarchar(60)",
                maxLength: 60,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "PhoneNumber",
                table: "User",
                type: "nvarchar(60)",
                maxLength: 60,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "ZipCode",
                table: "User",
                type: "nvarchar(60)",
                maxLength: 60,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<double>(
                name: "TotalCost",
                table: "Shipment",
                type: "float",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "TotalWeight",
                table: "Shipment",
                type: "float",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "receiverName",
                table: "Shipment",
                type: "nvarchar(60)",
                maxLength: 60,
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_ShipmentStatus_Shipment_ShipmentId",
                table: "ShipmentStatus",
                column: "ShipmentId",
                principalTable: "Shipment",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ShipmentStatus_Status_StatusId",
                table: "ShipmentStatus",
                column: "StatusId",
                principalTable: "Status",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ShipmentStatus_Shipment_ShipmentId",
                table: "ShipmentStatus");

            migrationBuilder.DropForeignKey(
                name: "FK_ShipmentStatus_Status_StatusId",
                table: "ShipmentStatus");

            migrationBuilder.DropColumn(
                name: "Country",
                table: "User");

            migrationBuilder.DropColumn(
                name: "DateOfBirth",
                table: "User");

            migrationBuilder.DropColumn(
                name: "PhoneNumber",
                table: "User");

            migrationBuilder.DropColumn(
                name: "ZipCode",
                table: "User");

            migrationBuilder.DropColumn(
                name: "TotalCost",
                table: "Shipment");

            migrationBuilder.DropColumn(
                name: "TotalWeight",
                table: "Shipment");

            migrationBuilder.DropColumn(
                name: "receiverName",
                table: "Shipment");

            migrationBuilder.RenameColumn(
                name: "StatusId",
                table: "ShipmentStatus",
                newName: "StatusesId");

            migrationBuilder.RenameColumn(
                name: "ShipmentId",
                table: "ShipmentStatus",
                newName: "ShipmentsId");

            migrationBuilder.RenameIndex(
                name: "IX_ShipmentStatus_StatusId",
                table: "ShipmentStatus",
                newName: "IX_ShipmentStatus_StatusesId");

            migrationBuilder.AddForeignKey(
                name: "FK_ShipmentStatus_Shipment_ShipmentsId",
                table: "ShipmentStatus",
                column: "ShipmentsId",
                principalTable: "Shipment",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ShipmentStatus_Status_StatusesId",
                table: "ShipmentStatus",
                column: "StatusesId",
                principalTable: "Status",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
