﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Boxinator.Migrations
{
    public partial class keycloak : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "KeyCloakId",
                table: "User",
                type: "nvarchar(60)",
                maxLength: 60,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "KeyCloakId",
                table: "User");
        }
    }
}
