﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Boxinator.Migrations
{
    public partial class keycloak2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: 1,
                column: "KeyCloakId",
                value: "testkey");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: 1,
                column: "KeyCloakId",
                value: null);
        }
    }
}
