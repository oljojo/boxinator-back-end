﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Boxinator.Migrations
{
    public partial class setting_nullable_foreignkey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Package_Country_CountryId",
                table: "Package");

            migrationBuilder.DropForeignKey(
                name: "FK_Package_Shipment_ShipmentId",
                table: "Package");

            migrationBuilder.DropForeignKey(
                name: "FK_Package_Tier_TierId",
                table: "Package");

            migrationBuilder.DropForeignKey(
                name: "FK_Shipment_User_UserId",
                table: "Shipment");

            migrationBuilder.AlterColumn<int>(
                name: "UserId",
                table: "Shipment",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "TierId",
                table: "Package",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "ShipmentId",
                table: "Package",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "CountryId",
                table: "Package",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_Package_Country_CountryId",
                table: "Package",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Package_Shipment_ShipmentId",
                table: "Package",
                column: "ShipmentId",
                principalTable: "Shipment",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Package_Tier_TierId",
                table: "Package",
                column: "TierId",
                principalTable: "Tier",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Shipment_User_UserId",
                table: "Shipment",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Package_Country_CountryId",
                table: "Package");

            migrationBuilder.DropForeignKey(
                name: "FK_Package_Shipment_ShipmentId",
                table: "Package");

            migrationBuilder.DropForeignKey(
                name: "FK_Package_Tier_TierId",
                table: "Package");

            migrationBuilder.DropForeignKey(
                name: "FK_Shipment_User_UserId",
                table: "Shipment");

            migrationBuilder.AlterColumn<int>(
                name: "UserId",
                table: "Shipment",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "TierId",
                table: "Package",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ShipmentId",
                table: "Package",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CountryId",
                table: "Package",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Package_Country_CountryId",
                table: "Package",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Package_Shipment_ShipmentId",
                table: "Package",
                column: "ShipmentId",
                principalTable: "Shipment",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Package_Tier_TierId",
                table: "Package",
                column: "TierId",
                principalTable: "Tier",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Shipment_User_UserId",
                table: "Shipment",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
