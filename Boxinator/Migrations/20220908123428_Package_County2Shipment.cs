﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Boxinator.Migrations
{
    public partial class Package_County2Shipment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Package_Country_CountryId",
                table: "Package");

            migrationBuilder.DropIndex(
                name: "IX_Package_CountryId",
                table: "Package");

            migrationBuilder.DropColumn(
                name: "CountryId",
                table: "Package");

            migrationBuilder.AddColumn<int>(
                name: "CountryId",
                table: "Shipment",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Shipment_CountryId",
                table: "Shipment",
                column: "CountryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Shipment_Country_CountryId",
                table: "Shipment",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Shipment_Country_CountryId",
                table: "Shipment");

            migrationBuilder.DropIndex(
                name: "IX_Shipment_CountryId",
                table: "Shipment");

            migrationBuilder.DropColumn(
                name: "CountryId",
                table: "Shipment");

            migrationBuilder.AddColumn<int>(
                name: "CountryId",
                table: "Package",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Package_CountryId",
                table: "Package",
                column: "CountryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Package_Country_CountryId",
                table: "Package",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id");
        }
    }
}
